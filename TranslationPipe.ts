import {PipeTransform} from '@angular/core';
import {MainService} from "./src/services/main.service";


export abstract class TranslationPipe implements PipeTransform {

  dictioniaries: any = {};
  default: string;

  protected constructor (protected mainService: MainService){}

  addDictionary(lang: string, dict: any){
    this.dictioniaries[lang] = dict;
  }

  setDefault(lang: string){
    this.default = lang;
  }

  transform(value: any, ...args: any[]): any {

    const lang = this.mainService.getLanguage();
    if (!value) return value;

    // object with translations
    if (!(typeof value === 'string')){
      if (value.hasOwnProperty(lang)){
        return value[lang];
      } else {
        return value[Object.keys(value)[0]];
      }
    }

    // translate string
    if (this.dictioniaries[lang]){
      if (this.dictioniaries[lang][value]){
        return this.dictioniaries[lang][value]
      } else {
        if (this.dictioniaries[this.default][value]){
          return this.dictioniaries[this.default][value];
        } else {
          return "@" + value;
        }
      }
    } else {
      if (this.dictioniaries[this.default][value]){
        return this.dictioniaries[this.default][value];
      } else {
        return "@" + value;
      }
    }

  }

}
