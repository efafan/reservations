import {isDevMode} from '@angular/core';
import {APIService} from "./APIService";
import {MainService} from "./main.service";


export function APIFactory(APIService: APIService, mainService: MainService) {

  APIService.setEndPoint('http://localhost:3000');

  let configUrl = (isDevMode()) ? null : "config.json";

  return () => APIService.init(configUrl).then(response => {
    if (response && response.user){
      mainService.setUser(response.user);
    }

    // if (response && response.options){
    //   mainService.setOptions(response.options);
    // }

    console.log('Juice init');
  });


}
