import {HttpClient, HttpHeaders} from '@angular/common/http'
import {Injectable, isDevMode} from "@angular/core";
import {MainService} from "./main.service";

@Injectable()
export class APIService {

  private endpoint: string;
  private token: string;

  constructor(protected http: HttpClient,
              private mainService: MainService) {
   console.log('connected');
  }


  getEndPoint(){
    return this.endpoint;
  }

  setEndPoint(url){
    this.endpoint = url;
  }

  loadConfiguration(path: string): Promise<any>{
    return new Promise((resolve, reject) => {
      this.http.get(path).subscribe(data => {
        this.setEndPoint((<any>data).url);
        resolve(true);
      }, error => {
        resolve(null);
      });
    });
  }

  init(config?: string): Promise<any> {
    return new Promise((resolve, reject) => {
      if(config){
        this.loadConfiguration(config).then(success => {
          if (success){
            this.requestToken().then(user => {
              // if (user){
              //   this.http.get( this.getEndPoint() + '/booking/options').subscribe(options=>{
              //     resolve({ user: user, options: options });
              //   });
              // } else
                resolve({ user: user });
            });
          } else {
            reject("Failed fetching configuration");
          }
        })
      } else {
        this.requestToken().then(user => {
          // if (user){
          //   this.http.get( this.getEndPoint() + '/booking/options').subscribe(options=>{
          //     resolve({ user: user, options: options });
          //   });
          // } else
            resolve({ user: user });
        })
      }
    });
  }

  private requestToken(): Promise<any> {
    return new Promise((resolve, reject) => {

      const token = this.getToken();
      if (!token)
        return resolve(false);

      let headers = new HttpHeaders();
      headers = headers.append('juice-token', token);

      if (typeof token != "undefined" || token != null) {
        this.http.get(this.endpoint + '/booking/validate-token', {
          headers: headers
        }).subscribe(response => {
          if ((response as any).success) {
            return resolve((response as any).user);
          } else {
            localStorage.removeItem("juice_token");
            return resolve(null);
          }
        });
      }
    })
  }

  login(email: string, password: string){
    return new Promise((resolve, reject) => {

      this.http.post(this.endpoint + '/booking/auth', {
        email: email,
        password: password
      }).subscribe(response => {
        console.log(response);
        if ((response as any).success) {
          this.setToken((response as any).token);
          return resolve(true);
        } else {
          return resolve(false);
        }
      }, error => {
        return resolve(false);
      });

    });
  }

  getToken(): string {
    try {
      const token = localStorage.getItem("juice_token");
      return JSON.parse(token);
    } catch (exception) {
      localStorage.clear();
      return null;
    }
  }

  setToken(token: string) {
    localStorage.setItem("juice_token", JSON.stringify(token));
  }

  isLoggedIn(){
    return this.getToken() != null;
  }
  logout() {
    window.localStorage.clear();
    window.location.reload();
  }

}
