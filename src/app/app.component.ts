import {Component, OnInit} from '@angular/core';
import {APIService} from "../services/APIService";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent{
  constructor(private APIservice: APIService){
    this.APIservice.setEndPoint('http://localhost:3000')
  }

  title = 'reservations-app';
}
