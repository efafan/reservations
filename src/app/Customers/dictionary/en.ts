export const en = {

  'name':'Name',
  'include_name':'Include name',
  'number_of_persons':'Number of persons',
  'select_startdate':'Select start date',
  'select_enddate': 'Select end date',
  'send_request':'Send request',
  'title':'Title',
  'asset_name':'Asset name',
  'issue':'Issue',
  'firstname':'Firstname',
  'lastname':'Lastname',
  'email':'Email',
  'password':'Password'
};
