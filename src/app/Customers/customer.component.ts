import {Component, OnInit} from '@angular/core';
import {APIService} from "../../services/APIService";
import {InquiresService} from "../Inquires/inquires.service";
import {MainService} from "../../services/main.service";
import {Router} from "@angular/router";
import {CustomersService} from "./customers.service";
import {CustomerTranslation} from "./dictionary/customer.translation";

@Component({
  selector: 'app-customer',
  templateUrl: './customer.component.html'
})
export class CustomerComponent implements OnInit {


  public rows :Array<any>;
  public columns: Array<any>;

  promiseBtn: any;

  constructor(private apiService: APIService,
              private customerService:CustomersService,
              private mainService: MainService,
              public router: Router) {
  }

  ngOnInit() {

    const i18n = new CustomerTranslation(this.mainService);

    this.columns = [
      {name: i18n.transform('firstname'), sort: 'asc',
        prop:'firstname'},
      {name: i18n.transform('lastname'), sort: 'asc',
        prop:'lastname'},
      {name: i18n.transform('email'), sort: 'asc',
        prop:'email'}
    ];

    this.customerService.fetch().then(customers=>{
      this.rows=(<any> customers).customers;
      console.log(this.rows);
    })
  }

  onSelect(event) {
    this.router.navigate(["create-reservation", event._id]);
  }

  getAssets(event){
    this.router.navigate(["show-reservation", event._id]);
  }


}
