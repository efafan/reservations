import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {APIService} from "../../../services/APIService";
import {InquiresService} from "../../Inquires/inquires.service";
import {MainService} from "../../../services/main.service";
import {CustomerTranslation} from "../dictionary/customer.translation";
import {ReservationService} from "../../Reservations/reservation.service";

@Component({
  selector: 'app-customer-reservations',
  templateUrl: './customer-reservations.component.html'
})
export class CustomerReservationsComponent implements OnInit {


  public rows :Array<any>;
  public columns: Array<any>;
  public customer_id: any;
  public user: any;


  constructor(private apiService: APIService,
              private inquireService:InquiresService,
              private mainService: MainService,
              public router: Router,
              public reservationService: ReservationService) {
  }

  async ngOnInit() {

    const i18n = new CustomerTranslation(this.mainService);
    console.log('usla');

    //Dobavi usera koji se kreirao kada je customer poslao inquiry, taj user dobije customer_id od customera koji je povezan s inquirijem i preko toga
    //dobijes sve inquirije od tog usera, odnosno customera, isprobaj kada se node popravi

    this.user= await this.mainService.getUser();

    this.columns = [
      {name: i18n.transform('number_of_persons'), sort: 'asc',
        prop:'number_of_persons'},
      {name: i18n.transform('start_date'), sort: 'asc',
        prop:'startDate'},
      {name: i18n.transform('end_date'), sort: 'asc',
        prop:'endDate'}
    ];

    this.inquireService.getCustomerInquires(this.user.customer_id).then(inquires=>{
      for (let inquiry of (<any> inquires).inquiry){
        inquiry.startDate= inquiry.startDate.day +  '. ' + inquiry.startDate.month + '. ' + inquiry.startDate.year;
        inquiry.endDate= inquiry.endDate.day +  '. ' + inquiry.endDate.month + '. ' + inquiry.endDate.year;
      }
      this.rows=(<any> inquires).inquiry;
      console.log(this.rows);

    });



  }

  onSelect(event) {
    this.router.navigate(["user/one-reservation/", event.reservation_id]);
  }


}
