import {Injectable, OnInit} from '@angular/core';
import {APIService} from "../../services/APIService";
import {HttpClient} from "@angular/common/http";

@Injectable()
export class CustomersService{

  constructor(protected http: HttpClient,
              private API:APIService) { }

  /**
   * Returns the event specified by its id
   *
   * @param articleId
   * @returns {Promise<any>}
   */
  get(){
    return new Promise(resolve =>{
      const url = '/booking/test' ;
      return this.http.get( this.API.getEndPoint() + url).subscribe(res=>{
        console.log(res);
      });
    })

  }

  createCustomer(body){
    return new Promise(resolve =>{
      const url = '/booking/customers/create' ;
      return this.http.post( this.API.getEndPoint() + url, body).subscribe(res=>{
       resolve(res);
      });
    })

  }

  fetch(){
    return new Promise(resolve =>{
      const url = '/booking/customers/fetch' ;
      this.http.post( this.API.getEndPoint() + url, {}).subscribe(res=>{
        resolve(res);
      });
    })

  }

}


