import {Component, OnInit} from '@angular/core';
import {APIService} from "../../services/APIService";
import {InquiresService} from "../Inquires/inquires.service";
import {InquiryAdminTranslation} from "./dictionary/Inquiry-admin.translation";
import {MainService} from "../../services/main.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-inquires-admin',
  templateUrl: './inquires-admin.component.html'
})
export class InquiresAdminComponent implements OnInit {


  public rows :Array<any>;
  public columns: Array<any>;

  promiseBtn: any;

  constructor(private apiService: APIService,
              private inquireService:InquiresService,
              private mainService: MainService,
              public router: Router) {
  }

  ngOnInit() {

    const i18n = new InquiryAdminTranslation(this.mainService);
    console.log('usla');

  this.columns = [
    {name: i18n.transform('number_of_persons'), sort: 'asc',
      prop:'number_of_persons'},
    {name: i18n.transform('start_date'), sort: 'asc',
      prop:'startDate'},
    {name: i18n.transform('end_date'), sort: 'asc',
      prop:'endDate'}
    ];

    this.inquireService.fetch().then(inquires=>{
      this.rows=(<any> inquires).inquires;
      console.log(this.rows);
    })
  }

  onSelect(event) {
      this.router.navigate(["create-reservation", event._id]);
  }


}
