import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {APIService} from "../../services/APIService";


@Injectable()
export class ReservationService{

  constructor(protected http: HttpClient,
              private API:APIService) { }

  getNodeInfo(){
    return new Promise(resolve => {
      const url = '/booking/assets/node';
      return this.http.get(this.API.getEndPoint() + url).subscribe(res => {
        resolve(res);
      });
    })
  }

  // sell(entity_id: string, asset: string, quantity, payload: any): Promise<boolean>{
  //   const url = '/juicebox/assets/sell';
  //   return this.juicEShop.post(url, {
  //     entity_id: entity_id,
  //     asset: asset,
  //     quantity: quantity,
  //     payload: payload
  //   });
  // }

  transfer(user_id: string, asset: string, quantity, payload: string){
    return new Promise(resolve => {
      const url = '/booking/assets/transfer';
      return this.http.post(this.API.getEndPoint() + url,{
        user_id: user_id,
        asset: asset,
        quantity: quantity,
        payload: payload
      }).subscribe(res => {
        resolve(res);
      });
    })
  }


  /**
   * Returns the article specified by its id
   *
   * @param articleId
   * @returns {Promise<any>}
   */
  get(assetId){
    return new Promise(resolve => {
      const url = '/booking/assets/' + assetId;
      return this.http.get(this.API.getEndPoint() + url).subscribe(res => {
        resolve(res);
      });
    });
  }

  /**
   * Update articles properties
   *
   * @param articleId
   * @param data
   * @returns {Promise<any>}
   */
  put(ticketsId, data): Promise<any> {
    return new Promise(resolve => {
    const url = '/booking/assets/' + ticketsId;
    return this.http.put(this.API.getEndPoint() + url, data).subscribe(res => {
      resolve(res);
    });
  });
  }



  /**
   * Returns all assets for given category specified by id.
   *
   * @param page - specifies number of page needed assets are for and
   * @param pagesize - specifies number of assets per one page
   * @param category
   * @returns {Promise<any>}
   */
  // fetch(page, pagesize, options): Promise<any> {
  //   let params = page + '/' + pagesize;
  //   const url = '/juicebox/assets/fetch/' + params;
  //   return this.juicEShop.post(url, options);
  // }

  create(data, userId): Promise<any> {
    return new Promise(resolve => {
      const url = '/booking/assets/' + userId;
      return this.http.post(this.API.getEndPoint() + url, data).subscribe(res => {
        resolve(res);
      });
    });
  }

  public async updateDescription(assetName: string, description: string){
    return new Promise(resolve => {
      const url = '/booking/assets/description';
      return this.http.put(this.API.getEndPoint() + url, {
        description: description,
        name: assetName
      }).subscribe(res => {
        resolve(res);
      });
    });
  }

  public async getEntityAssets(customerId){
    return new Promise(resolve => {
      const url = '/booking/assets/entity/' + customerId;
      return this.http.get(this.API.getEndPoint() + url ).subscribe(res => {
        resolve(res);
      });
    });
  }

  public async getInquiryAsset(inquiryId){
    return new Promise(resolve => {
      const url = '/booking/assets/inquires/' + inquiryId;
      return this.http.get(this.API.getEndPoint() + url ).subscribe(res => {
        resolve(res);
      });
    });
  }

  public async sell(entity_id: string, asset: string, quantity, payload: string, userId: string){
  return new Promise(resolve=>{
  const url = '/booking/assets/sell/' + userId;
  return this.http.post(this.API.getEndPoint() + url, {
    entity_id: entity_id,
    asset: asset,
    quantity: quantity,
    payload: payload
    }).subscribe(res => {
    resolve(res);
  });
  })

  }


}


