import {Component, OnInit} from "@angular/core";
import {ActivatedRoute, Router} from '@angular/router';
import {DatePipe} from '@angular/common';
import {CustomersService} from "../../Customers/customers.service";
import {MainService} from "../../../services/main.service";
import {ReservationService} from "../reservation.service";
import {ReservationTranslation} from "../dictionary/Reservation.translation";

@Component({
  selector: 'app-show-reservation',
  templateUrl: './show-reservation.component.html'
})
export class ShowReservationComponent implements OnInit{

  public language: string;
  public tickets: any;
  public loader: boolean = true;
  public columns: Array<any>;
  public customer_id: any;

  public constructor(private customerService: CustomersService,
                     private router: Router,
                     protected mainService: MainService,
                     private reservationService: ReservationService,
                     public activated: ActivatedRoute) {

    this.customer_id = (<any> activated.pathFromRoot[2].params).value.id;
  }


  public ngOnInit(): void {

    const i18n = new ReservationTranslation(this.mainService);
    this.columns = [
      {name: i18n.transform('title'), prop: "title.de_DE", width: 400 },
      {name: i18n.transform('quantity'), prop: "quantity" }
    ];


      this.reservationService.getEntityAssets(this.customer_id).then(tickets => {
        this.tickets = tickets;
        console.log(this.tickets);
        this.loader = false;
      });

  }

  onSelect(){

  }
}
