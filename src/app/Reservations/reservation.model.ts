

export class Reservation {
  title: string;
  inquiry_id: string;
  user_id: string;
  quantity: string;
  number: string;
  amount: string;
  name: string;
  validTill: string;
  onDate: string;
  endDate: string;
}
