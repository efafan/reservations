import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators} from '@angular/forms';
import {ReservationService} from "./reservation.service";
import {MainService} from "../../services/main.service";
import {InquiresService} from "../Inquires/inquires.service";
import {ActivatedRoute, Router} from "@angular/router";
import {ReservationTranslation} from "./dictionary/Reservation.translation";
import {Reservation} from "./reservation.model";
import {IMyDateModel, INgxMyDpOptions, NgxMyDatePickerConfig} from "ngx-mydatepicker";

@Component({
  selector: 'app-create-reservations',
  templateUrl: './create-reservations.component.html'
})
export class CreateReservationsComponent implements OnInit {

  public reservationForm: FormGroup;
  public updatePromise = null;
  public reservation: any;
  public language: any;
  public inquires_id:any;
  public inquiry:any;
  public selectedDateStart:any;
  public selectedDateEnd: any;

  public node: any;
  public publisher: any;
  // myOptions: INgxMyDpOptions = {
  //   // other options...
  //   dateFormat: 'dd.mm.yyyy',
  // };


  constructor(
    private reservationService: ReservationService,
    private inquiresService: InquiresService,
    private router: Router,
    public mainService: MainService,
    private activated :ActivatedRoute,
    public config: NgxMyDatePickerConfig) {

    const i18n = new ReservationTranslation(this.mainService);
    this.inquires_id = (<any> activated.pathFromRoot[2].params).value.id;

  }

 async ngOnInit() {

   this.reservation = new Reservation();
   this.reservation.title = {de_DE: ""};
   this.reservation.validTill = "";
   this.inquiry={
     inquires:{
       startDate:"",
       endDate:""
     }
   };


   this.reservationForm = new FormGroup({
     title: new FormControl('', Validators.required),
     customName: new FormControl( '', Validators.required),
     onDate: new FormControl('', Validators.required),
     endDate: new FormControl('', Validators.required),
   });
    console.log(this.inquires_id);
    this.language = this.mainService.getLanguage();

    if (!this.node) this.node = "";

    this.inquiry= await this.inquiresService.get(this.inquires_id);
    this.inquiry.inquires.startDate={date:this.inquiry.inquires.startDate};
    this.inquiry.inquires.endDate={date:this.inquiry.inquires.endDate};
    console.log(this.inquiry.inquires);




    this.reservationService.getNodeInfo().then(nodeInfo => {
      this.node = nodeInfo;
    });


    this.publisher = this.mainService.getUser();
  }

  onDateChangedStart(event: IMyDateModel): void {
    if(event.formatted !== '') {
      // save valid date
      this.selectedDateStart = event.date;
      console.log(this.selectedDateStart);
    }
  }

  onDateChangedEnd(event: IMyDateModel): void {
    if(event.formatted !== '') {
      // save valid date
      this.selectedDateEnd = event.date;
      console.log(this.selectedDateEnd);
    }
  }


  // serviceName(name){
  //   this.asset.customName= this.organisation.name.toLowerCase().replace(/ /g,"") + ':' + name.toLowerCase().replace(/ /g,"");
  // }
  // startDate(date){
  //   this.config.minDate = date;
  //   this.config.outsideDays = 'hidden';
  // }

  publish(){
    Object.keys(this.reservationForm.controls).map((controlName) => {
      this.reservationForm.get(controlName).markAsTouched({onlySelf: true});
      if (this.reservationForm.get(controlName).invalid){
        console.log(controlName);
      }
    });

//TODO alert
    if (this.reservation.customName.length > 32){

        console.log("Creating Asset Failed","Unique identifier (Asset name) is too long");

    }
    if (this.reservationForm.invalid)
      return;

    let date = null;
    if (this.reservation.onDate) {
      // date = new Date(this.asset.onDate.year, this.asset.onDate.month - 1,
      //   this.asset.onDate.day);
    }

    let enddate = null;
    if (this.reservation.endDate) {
      // enddate = new Date(this.asset.endDate.year, this.asset.endDate.month - 1,
      //   this.asset.endDate.day);
    }

    this.updatePromise = (async () => {
      const lang = this.mainService.getLanguage();
      let asset =await this.reservationService.create({
        name: this.reservation.customName,
        title: this.reservation.title,
        //number: this.asset.number,
        amount: 1,
        inquiry_id: this.inquires_id,
        publisher: this.publisher.email,
        organisation_id: this.publisher._id,
        publisher_id: this.publisher._id,
        onDate: this.selectedDateStart,
        endDate: this.selectedDateEnd,
        created_id: this.inquiry.inquires.customer_id,
        type: 'voucher'
      }, this.publisher._id);

      if (asset.response.success){
        let sell = await this.reservationService.sell( this.inquiry.inquires.customer_id.toString(), asset.asset.name,
          1, 'reservation', this.publisher._id);
        console.log(sell);

        let inquiry= await this.inquiresService.update(this.inquires_id, {
          reservation_id: asset.asset._id
        });
        console.log(inquiry);
      }






    })();
  }

}
