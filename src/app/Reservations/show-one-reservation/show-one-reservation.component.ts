import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {APIService} from "../../../services/APIService";
import {InquiresService} from "../../Inquires/inquires.service";
import {MainService} from "../../../services/main.service";
import {ReservationService} from "../reservation.service";
import {ReservationTranslation} from "../dictionary/Reservation.translation";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {Reservation} from "../reservation.model";

@Component({
  selector: 'app-show-one-reservation',
  templateUrl: './show-one-reservation.component.html'
})
export class ShowOneReservationComponent implements OnInit {


  public customer_id: any;
  public reservation: any;
  public reservation_id:any;
  public reservationForm: FormGroup;


  constructor(private apiService: APIService,
              private inquireService:InquiresService,
              private mainService: MainService,
              public router: Router,
              public reservationService: ReservationService,
              public activated: ActivatedRoute) {
    this.reservation_id = (<any> activated.pathFromRoot[2].params).value.id;
  }

  async ngOnInit() {

    const i18n = new ReservationTranslation(this.mainService);


   this.reservation=new Reservation();
    this.reservation.title = {de_DE: ""};
    this.reservation.validTill = "";



    this.reservationService.get(this.reservation_id).then(reservation=>{
      this.reservation=reservation;

    });



  }

}
