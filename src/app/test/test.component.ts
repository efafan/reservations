import { Component, OnInit } from '@angular/core';
import {TestService} from "./test.service";

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html'
})
export class TestComponent implements OnInit {

  public test:any;

  constructor(public testService:TestService) { }

  async ngOnInit() {

    this.test= await this.testService.get();
  }

}
