import {Injectable, OnInit} from '@angular/core';
import {APIService} from "../../services/APIService";
import {HttpClient} from "@angular/common/http";

@Injectable()
export class TestService{

  constructor(protected http: HttpClient,
              private API:APIService) { }

  /**
   * Returns the event specified by its id
   *
   * @param articleId
   * @returns {Promise<any>}
   */
  get(){
    return new Promise(resolve =>{
      const url = '/booking/test' ;
      return this.http.get( this.API.getEndPoint() + url).subscribe(res=>{
        console.log(res);
      });
    })

  }

}


