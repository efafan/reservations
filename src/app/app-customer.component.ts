import {Component, OnInit} from '@angular/core';
import {APIService} from "../services/APIService";

@Component({
  selector: 'app-customer-root',
  templateUrl: './app-customer.component.html'
})
export class AppCustomerComponent{
  constructor(private APIservice: APIService){
    this.APIservice.setEndPoint('http://localhost:3000')
  }

  title = 'reservations-app';
}
