import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import {APIService} from "../services/APIService";
import {MainService} from "../services/main.service";

@Injectable()
export class AuthGuardServiceCustomer implements CanActivate {

  constructor(
    private router: Router,
    private apiService: APIService,
    private mainService: MainService
  ) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    const isLoggedIn = this.apiService.isLoggedIn(); // ... your login logic here
    const user= this.mainService.getUser();
    if (isLoggedIn && user.roles=="customer") {
      return true;
    } else {
      this.router.navigate(['/inquires']);
      return false;
    }
  }

}
