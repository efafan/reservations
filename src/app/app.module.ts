import { BrowserModule } from '@angular/platform-browser';
import {APP_INITIALIZER, NgModule} from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {APIService} from "../services/APIService";
import {TestComponent} from "./test/test.component";
import {RouterModule, Routes} from "@angular/router";
import {HttpClientModule} from "@angular/common/http";
import {TestService} from "./test/test.service";
import {LoginComponent} from "./login/login.component";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {NavigationComponent} from "./navigation/navigation.component";
import {InquiryTranslation} from "./Inquires/dictionary/inquiry.translation";
import {InquiresComponent} from "./Inquires/inquires.component";
import {InquiresService} from "./Inquires/inquires.service";
import {MainService} from "../services/main.service";
import {NgxMyDatePickerModule} from "ngx-mydatepicker";
import {CustomersService} from "./Customers/customers.service";
import {AuthGuardService} from "./AuthGuardService";
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import {InquiresAdminComponent} from "./Inquires-admin/inquires-admin.component";
import {UserService} from "./Users/user.service";
import {APIFactory} from "../services/APIFactory";
import {InquiryAdminTranslation} from "./Inquires-admin/dictionary/Inquiry-admin.translation";
import {ReservationService} from "./Reservations/reservation.service";
import {ReservationTranslation} from "./Reservations/dictionary/Reservation.translation";
import {CreateReservationsComponent} from "./Reservations/create-reservations.component";
import {CustomerTranslation} from "./Customers/dictionary/customer.translation";
import {CustomerComponent} from "./Customers/customer.component";
import {ShowReservationComponent} from "./Reservations/ShowReservation/show-reservation.component";
import {CustomerReservationsComponent} from "./Customers/customer-reservations/customer-reservations.component";
import {AuthGuardServiceCustomer} from "./AuthGuardServiceCustomer";
import {AppCustomerComponent} from "./app-customer.component";
import {NavigationCustomerComponent} from "./navigation/navigation-customer/navigation-customer.component";
import {ShowOneReservationComponent} from "./Reservations/show-one-reservation/show-one-reservation.component";

const appRoutes: Routes = [

  {
    path: '', component: AppComponent,
    canActivate:[AuthGuardService],
    children: [
      {path: 'inquires-admin', component: InquiresAdminComponent},
      {path: 'create-reservation/:id', component: CreateReservationsComponent},
      {path: 'customers', component: CustomerComponent},
      {path: 'show-reservation/:id', component: ShowReservationComponent},
    ]
  },
  {
    path: 'user', component: AppComponent,
    canActivate:[AuthGuardServiceCustomer],
    children: [
      {path: 'all-reservations', component: CustomerReservationsComponent},
      {path: 'one-reservation/:id', component: ShowOneReservationComponent}
    ]
  },
  {
    path:'inquires',
    component: InquiresComponent
  },
  {
    path: 'login',
    component: LoginComponent
  }
];

@NgModule({
  declarations: [
    AppComponent,
    TestComponent,
    LoginComponent,
    NavigationComponent,
    InquiryTranslation,
    InquiresComponent,
    InquiresAdminComponent,
    InquiryAdminTranslation,
    ReservationTranslation,
    CreateReservationsComponent,
    CustomerTranslation,
    CustomerComponent,
    ShowReservationComponent,
    CustomerReservationsComponent,
    AppCustomerComponent,
    NavigationCustomerComponent,
    ShowOneReservationComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    RouterModule.forRoot(appRoutes),
    HttpClientModule,
    ReactiveFormsModule,
    NgxMyDatePickerModule.forRoot(),
    NgxDatatableModule,
    FormsModule
  ],
  providers: [APIService,
              TestService,
              InquiresService,
              MainService,
    { provide: APP_INITIALIZER, useFactory: APIFactory, deps: [APIService, MainService], multi: true },
            CustomersService,
            AuthGuardService,
    AuthGuardServiceCustomer,
            UserService,
            ReservationService]
  ,
  bootstrap: [AppComponent]
})
export class AppModule { }
