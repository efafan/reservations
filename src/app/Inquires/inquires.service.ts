import {Injectable, OnInit} from '@angular/core';
import {APIService} from "../../services/APIService";
import {HttpClient} from "@angular/common/http";

@Injectable()
export class InquiresService{

  constructor(protected http: HttpClient,
              private API:APIService) { }

  /**
   * Returns the event specified by its id
   *
   * @param articleId
   * @returns {Promise<any>}
   */
  get(inquiresId){
    return new Promise(resolve =>{
      const url = '/booking/inquires/' + inquiresId;
      return this.http.get( this.API.getEndPoint() + url).subscribe(res=>{
        resolve(res);
      });
    })

  }

  getCustomerInquires(customerId){
    return new Promise(resolve =>{
      const url = '/booking/inquires/customer/' + customerId;
      return this.http.get( this.API.getEndPoint() + url).subscribe(res=>{
        resolve(res);
      });
    })

  }

  sendRequest(body){
    return new Promise(resolve =>{
      const url = '/booking/inquires/create' ;
      this.http.post( this.API.getEndPoint() + url, body).subscribe(res=>{
        resolve(res);
      });
    })

  }

  fetch(){
    return new Promise(resolve =>{
      const url = '/booking/inquires/fetch' ;
      this.http.post( this.API.getEndPoint() + url, {}).subscribe(res=>{
        resolve(res);
      });
    })

  }

  update(inquiresId, data){
    return new Promise(resolve =>{
      const url = '/booking/inquires/update/' + inquiresId;
      return this.http.put( this.API.getEndPoint() + url, data).subscribe(res=>{
        resolve(res);
      });
    })

  }

}


