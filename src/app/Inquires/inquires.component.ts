import {Component, Input, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {InquiresService} from "./inquires.service";
import {MainService} from "../../services/main.service";
import {InquiryTranslation} from "./dictionary/inquiry.translation";
import {IMyDateModel, INgxMyDpOptions} from "ngx-mydatepicker";
import {CustomersService} from "../Customers/customers.service";
import {UserService} from "../Users/user.service";



@Component({
  selector: 'app-inquires',
  templateUrl: './inquires.component.html'
})
export class InquiresComponent implements OnInit {

  public inquiryForm: FormGroup;
  public language: string;
  public inquiry: any;
  public personalData:boolean;
  public requestButton:boolean;
  private selectedDateStart:any;
  private selectedDateEnd:any;
  public publisher: any;
  // myOptions: INgxMyDpOptions = {
  //   // other options...
  //   dateFormat: 'dd.mm.yyyy',
  // };



  constructor(private inquiresService: InquiresService,
              public mainService: MainService,
              private router: Router,
              public customerService:CustomersService,
              public userService: UserService) {
    this.language = mainService.getLanguage();

    this.inquiry = {
      name: '',
      startDate:0,
      endDate:0,
      quantity:0
    };

    const i18n = new InquiryTranslation(this.mainService);
  }


  ngOnInit() {

    this.inquiryForm = new FormGroup({
      startDate: new FormControl('', Validators.required),
      endDate: new FormControl('', Validators.required),
      quantity: new FormControl('', Validators.required),
      firstname:new FormControl('', Validators.required),
      lastname:new FormControl('', Validators.required),
      email:new FormControl('', Validators.required),
      password: new FormControl('')
    });

    this.personalData=false;
    this.requestButton=true;

    this.publisher = this.mainService.getUser();
    console.log(this.publisher);

  }

  personalDataForm(){
    this.personalData=true;
    this.requestButton=false;
  }

  onDateChangedStart(event: IMyDateModel): void {
    if(event.formatted !== '') {
      // save valid date
      this.selectedDateStart = event.date;
      console.log(this.selectedDateStart);
    }
  }

  onDateChangedEnd(event: IMyDateModel): void {
    if(event.formatted !== '') {
      // save valid date
      this.selectedDateEnd = event.date;
      console.log(this.selectedDateEnd);
    }
  }



  async create() {
    // Object.keys(this.raffleForm.controls).map((controlName) => {
    //   this.raffleForm.get(controlName).markAsTouched({onlySelf: true});
    // });
    //
    // if (this.raffleForm.invalid)
    //   return;
    //
    let customer= await this.customerService.createCustomer({
      firstname: this.inquiry.firstname,
      lastname: this.inquiry.lastname,
      email: this.inquiry.email
    });
    console.log(customer);

      let request= await this.inquiresService.sendRequest({
        startDate: this.selectedDateStart,
        endDate: this.selectedDateEnd,
        quantity: this.inquiry.quantity,
        customer_id:( <any> customer).customer._id
      });

      console.log('request:', request);

      let user= await this.userService.createUserFromCustomer({
        customer_id: ( <any> customer).customer._id,
        email: ( <any> customer).customer.email,
        password: this.inquiry.password,
        firstname: this.inquiry.firstname,
        lastname: this.inquiry.lastname

      })


    //   }).then(success=>{
    //     this.activeModal.close('Modal Closed');
    //   });
    //
    // })();

  }



}
