import { Pipe, PipeTransform} from '@angular/core';
import {TranslationPipe} from "../../../../TranslationPipe";
import {MainService} from "../../../services/main.service";
import {en} from "./en";




@Pipe({name: 'translate'})
export class InquiryTranslation extends TranslationPipe {
  constructor(mainService: MainService) {
    super(mainService);

    this.setDefault("en_GB");
    this.addDictionary("en_GB", en);
  }
}
