import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {APIService} from "../../../services/APIService";

@Component({
  selector: 'app-navigation-customer',
  templateUrl: './navigation-customer.component.html'
})
export class NavigationCustomerComponent implements OnInit {

  public isLoggedIn:boolean;

  constructor(public router: Router,
              public APIService: APIService) {
  }

  async ngOnInit() {
    this.isLoggedIn=false;
    this.isLoggedIn=this.APIService.isLoggedIn();
    console.log(this.isLoggedIn);

  }


  logout(){
    this.APIService.logout();
  }



}
