import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {APIService} from "../../services/APIService";
import {MainService} from "../../services/main.service";

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html'
})
export class NavigationComponent implements OnInit {

  public isLoggedIn:boolean;
  public user: any;

  constructor(public router: Router,
              public APIService: APIService,
              private mainService: MainService) {
  }

  async ngOnInit() {
    this.isLoggedIn=false;
    this.isLoggedIn=this.APIService.isLoggedIn();

    this.user=this.mainService.getUser();
    console.log(this.user);

  }

async  goTo(){
    await this.router.navigateByUrl('/inquires-admin');
  }

  logout(){
    this.APIService.logout();
  }



}
