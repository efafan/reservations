import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import {APIService} from "../services/APIService";
import {MainService} from "../services/main.service";


@Injectable()
export class AuthGuardService implements CanActivate {

  constructor(
    private router: Router,
    private apiService: APIService,
    private mainService: MainService
  ) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    const isLoggedIn = this.apiService.isLoggedIn(); // ... your login logic here
    const user= this.mainService.getUser();
    console.log(user);
    if (isLoggedIn && user.roles=="admin") {
      return true;
    } else {
      this.router.navigate(['/user/all-reservations']);
      // this.router.navigate(['/login']);
      // return false;
    }
  }

}
