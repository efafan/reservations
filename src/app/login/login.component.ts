import {Component, OnInit} from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {APIService} from "../../services/APIService";
import {MainService} from "../../services/main.service";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html'
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  model: any = {};
  loading = false;
  returnUrl: string;
  showError = false;

  promiseBtn: any;

  constructor(fb: FormBuilder,
              private router: Router,
              private route: ActivatedRoute,
              private apiService: APIService,
              private mainService: MainService) {
    this.loginForm = fb.group({
      'email': [null, Validators.required],
      'password': [null, Validators.required]
    });
  }

  ngOnInit() {
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
    console.log(this.mainService.getUser());
    if (this.apiService.isLoggedIn())
      window.location.href = '/inquires-admin';
  }

  login() {
    this.promiseBtn = (async () => {
      this.loading = true;
      this.showError = false;

      let success = await this.apiService.login(this.model.email, this.model.password);
      console.log(success);

      if (success) {
        document.location.replace("/inquires-admin");
      } else {
        this.showError = true;
        this.loading = false;
      }
    })();
  }

}
