import {Injectable, OnInit} from '@angular/core';
import {APIService} from "../../services/APIService";
import {HttpClient} from "@angular/common/http";

@Injectable()
export class UserService{

  constructor(protected http: HttpClient,
              private API:APIService) { }

  /**
   * Returns the event specified by its id
   *
   * @param userId
   * @returns {Promise<any>}
   */
  get(){
    return new Promise(resolve =>{
      const url = '/booking/users' ;
      return this.http.get( this.API.getEndPoint() + url).subscribe(res=>{
        resolve(res);
      });
    })

  }

  sendRequest(body){
    return new Promise(resolve =>{
      const url = '/booking/inquires/create' ;
      this.http.post( this.API.getEndPoint() + url, body).subscribe(res=>{
        resolve(res);
      });
    })

  }

  fetch(){
    return new Promise(resolve =>{
      const url = '/booking/inquires/fetch' ;
      this.http.post( this.API.getEndPoint() + url, {}).subscribe(res=>{
        resolve(res);
      });
    })

  }

  createUserFromCustomer(body){
    return new Promise(resolve =>{
      const url = '/booking/users/customer/create' ;
      this.http.post( this.API.getEndPoint() + url, body).subscribe(res=>{
        resolve(res);
      });
    })

  }

}


